# profile

> Profile fron end technical interview

## Build Setup

``` bash
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

Telegram & Instagram id: mosa_5445

Phone number: +98 9035436306
